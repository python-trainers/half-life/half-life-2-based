from typing import assert_never

from trainerbase.codeinjection import AllocatingCodeInjection

from gameselection import selected_game
from memory import (
    ammo_in_clip_offset,
    current_weapon_pointer,
    hp_offset,
    make_server_dll_aob,
    player_pointer,
    player_pointer_register,
    update_current_weapon_pointer_aob,
    update_player_pointer_aob,
)


# Synergy, server.dll + 0x107031, aob: 83 BE FC 04 00 00 00 7F 4B
update_current_weapon_pointer = AllocatingCodeInjection(
    update_current_weapon_pointer_aob,
    f"""
        mov [{current_weapon_pointer}], esi

        cmp dword [esi + {ammo_in_clip_offset}], 0x0
    """,
    original_code_length=7,
)

match selected_game:
    case selected_game.SYNERGY:
        # Synergy, server.dll + 0x100D0E, aob: 8B 84 B7 14 07 00 00 5F
        infinite_throwable = AllocatingCodeInjection(
            make_server_dll_aob("8B 84 B7 14 07 00 00 5F"),
            """
                mov eax, 3
                mov [edi + esi * 4 + 0x714], eax

                mov eax, [edi + esi * 4 + 0x714]
            """,
            original_code_length=7,
        )
    case selected_game.HL2:
        infinite_throwable = AllocatingCodeInjection(
            make_server_dll_aob("8B 84 BB D0 06 00 00"),
            """
                mov eax, 3
                mov [ebx + edi * 4 + 0x6D0], eax

                mov eax, [ebx + edi * 4 + 0x6D0]
            """,
            original_code_length=7,
        )

    case selected_game.SIN_EPISODES:
        infinite_throwable = AllocatingCodeInjection(
            make_server_dll_aob("8B 84 B7 4C 06 00 00 5F 5E C2 04 00 CC CC CC"),
            """
                mov eax, 3
                mov [edi + esi * 4 + 0x64C], eax

                mov eax, [edi + esi * 4 + 0x64C]
            """,
            original_code_length=7,
        )
    case _:
        assert_never(selected_game)


update_player_pointer = AllocatingCodeInjection(
    update_player_pointer_aob,
    f"""
        mov [{player_pointer}], {player_pointer_register}

        cmp dword [{player_pointer_register} + {hp_offset}], 0x0
    """,
    original_code_length=7,
)

match selected_game:
    case selected_game.HL2 | selected_game.SYNERGY:
        one_hit_kill = AllocatingCodeInjection(
            make_server_dll_aob("FF 90 E4 01 00 00 89 3E 5F B8 01 00 00 00 5E 8B"),
            f"""
                call dword [eax + 0x1E4]

                push eax
                mov eax, {player_pointer}
                cmp eax, 0
                je skip

                mov eax, [{player_pointer}]
                add eax, {hp_offset}

                cmp eax, esi
                je skip

                mov edi, 0

                skip:
                pop eax

                mov [esi], edi
            """,
            original_code_length=8,
        )
    case selected_game.SIN_EPISODES:
        one_hit_kill = AllocatingCodeInjection(
            make_server_dll_aob("89 BE 9C 00 00 00 F6 43 3B 01"),
            f"""
                push eax

                mov eax, [{player_pointer}]

                cmp eax, esi
                je skip

                mov edi, 0

                skip:
                pop eax

                mov [esi + 0x9C], edi
            """,
            original_code_length=8,
        )
    case _:
        assert_never(selected_game)
