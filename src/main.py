from trainerbase.main import run

from gui import run_menu
from injections import update_current_weapon_pointer, update_player_pointer


def on_initialized():
    update_current_weapon_pointer.inject()
    update_player_pointer.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
