from trainerbase.gameobject import GameInt
from trainerbase.memory import Address

from memory import ammo_in_clip_offset, current_weapon_pointer, hp_offset, player_pointer


current_weapon_address = Address(current_weapon_pointer, [0x0])
player_address = Address(player_pointer, [0x0])

ammo_in_clip = GameInt(current_weapon_address + ammo_in_clip_offset)
hp = GameInt(player_address + hp_offset)
