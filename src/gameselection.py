from enum import StrEnum
from pathlib import Path

from pymem.process import base_module
from trainerbase.process import pm


class SelectedGame(StrEnum):
    SYNERGY = "synergy.exe"
    HL2 = "hl2.exe"
    SIN_EPISODES = "SinEpisodes.exe"


base_module_info = base_module(pm.process_handle)

if base_module_info is None:
    raise RuntimeError("Failed to get the main module info")

selected_game = SelectedGame(Path(base_module_info.filename).name)
