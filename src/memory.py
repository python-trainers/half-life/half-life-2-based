from functools import partial
from typing import assert_never

from trainerbase.memory import AOBScan, allocate, get_module_address

from gameselection import selected_game


assert get_module_address("server.dll", delay=4, retry_limit=32)

make_server_dll_aob = partial(AOBScan, module_name="server.dll")

current_weapon_pointer = allocate()
player_pointer = allocate()


match selected_game:
    case selected_game.SYNERGY:
        ammo_in_clip_offset = 0x4FC
        hp_offset = 0xE8
        update_current_weapon_pointer_aob = make_server_dll_aob("83 BE FC 04 00 00 00 7F 4B")
        update_player_pointer_aob = make_server_dll_aob("83 BB E8 00 00 00 00 0F 8C DA")
        player_pointer_register = "ebx"
    case selected_game.HL2:
        ammo_in_clip_offset = 0x4AC
        hp_offset = 0xE0
        update_current_weapon_pointer_aob = make_server_dll_aob("83 BE AC 04 00 00 00 75 3A")
        update_player_pointer_aob = make_server_dll_aob("83 BB E0 00 00 00 00 0F 8C B5")
        player_pointer_register = "ebx"
    case selected_game.SIN_EPISODES:
        ammo_in_clip_offset = 0x4EC
        hp_offset = 0x9C
        update_current_weapon_pointer_aob = make_server_dll_aob("83 BE EC 04 00 00 00 75")
        update_player_pointer_aob = make_server_dll_aob("83 BE 9C 00 00 00 00 0F 8C")
        player_pointer_register = "esi"
    case _:
        assert_never(selected_game)
