from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.misc import SeparatorUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI

from injections import infinite_throwable, one_hit_kill
from objects import ammo_in_clip, hp


@simple_trainerbase_menu("Half-Life 2 Based", 800, 320)
def run_menu():
    add_components(
        GameObjectUI(hp, "HP", "Shift+Insert", "Insert", default_setter_input_value=1000),
        GameObjectUI(ammo_in_clip, "Ammo In Clip", "Shift+Home", "Home", default_setter_input_value=30),
        SeparatorUI(),
        CodeInjectionUI(infinite_throwable, "Infinite Throwable", "PageUp"),
        CodeInjectionUI(one_hit_kill, "One Hit Kill", "PageDown"),
        SeparatorUI(),
        SpeedHackUI(default_factor_input_value=0.45),
    )
